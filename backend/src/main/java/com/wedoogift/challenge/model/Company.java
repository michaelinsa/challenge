package com.wedoogift.challenge.model;

import com.wedoogift.challenge.model.exception.NotEnoughMoneyException;

public class Company {

    private int id;
    private String name;
    private Integer balance;

    public Company(int id, String name, Integer balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public void pay(Integer amount) throws NotEnoughMoneyException{
        if (amount > this.getBalance()) {
            throw new NotEnoughMoneyException(String.format("%s does not have enough funds", this.getName()));
        }
        this.balance = this.balance - amount;
    }
}
