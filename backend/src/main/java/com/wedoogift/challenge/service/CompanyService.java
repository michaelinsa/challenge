package com.wedoogift.challenge.service;

import com.wedoogift.challenge.model.Company;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class CompanyService {

    public CompanyService(){
        this.companies = new HashMap<>();
    }
    Map<Integer, Company> companies;

    public Company getCompanyById(int companyId){
        return this.companies.get(new Integer(companyId));
    }

    public void addCompany(Company company) {
        this.companies.put(company.getId(), company);
    }
}
