package com.wedoogift.challenge.service;

import com.wedoogift.challenge.model.Company;
import com.wedoogift.challenge.model.Data;
import com.wedoogift.challenge.model.Distribution;
import com.wedoogift.challenge.model.User;
import org.springframework.stereotype.Service;

@Service
public class DataService {
    private Data data;

    public DataService() {
        this.data = new Data();
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public User getUserById(int userId) {
        return data.getUserById(userId);
    }

    public Company getCompanyById(int companyId) {
        return this.data.getCompanyById(companyId);
    }

    public void addUser(User user) {
        this.data.addUser(user);
    }

    public void addCompany(Company company){
        this.data.addCompany(company);
    }

    public void addDistribution(Distribution distribution) {
        this.data.addDistribution(distribution);
    }

}
