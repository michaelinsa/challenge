package com.wedoogift.challenge.service;

import com.wedoogift.challenge.model.*;
import com.wedoogift.challenge.model.exception.NotEnoughMoneyException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DistributionService {

    public DistributionService() {
        this.distributions = new ArrayList<>();
    }

    List<Distribution> distributions;

    public Distribution distribute(int distributionId, Company company, User user, Card card) {
        Distribution distribution;
        try {
            company.pay(card.getAmount());
            user.addCard(card);
            user.computeBalance();
            distribution = new Distribution(distributionId, card.getAmount(), card.getStartDate(), user.getId(), company.getId(), card.getWalletId());
            distribution.setEnd_date(card.getExpirationDate());
            this.distributions.add(distribution);
            return distribution;

        }
        catch (NotEnoughMoneyException e) {
            return null;
        }
    }

    public List<Distribution> getDistributions() {
        return this.distributions;
    }

}
