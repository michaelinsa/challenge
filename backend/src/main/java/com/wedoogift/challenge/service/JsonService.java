package com.wedoogift.challenge.service;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wedoogift.challenge.model.Data;
import com.wedoogift.challenge.model.User;
import com.wedoogift.challenge.model.WalletType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class JsonService {

    static Logger LOGGER = LoggerFactory.getLogger(JsonService.class);

    private Data data;

    public JsonService(){
        this.data = new Data();
    }

    public void saveData(Data data, String dataFilePath) {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").setPrettyPrinting().addSerializationExclusionStrategy(new SpecificClassExclusionStrategy(User.class)).create();
        try (FileWriter fileWriter = new FileWriter(dataFilePath)){
            gson.toJson(data, fileWriter);
            LOGGER.info("saving data");
        } catch (IOException e) {
            LOGGER.error("error while saving data");
        }
    }

    public Data loadData(String filePath) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileReader fileReader = new FileReader(filePath)) {
            this.data = gson.fromJson(fileReader, Data.class);
            LOGGER.info("loading data OK");
        } catch (FileNotFoundException fnfe) {
            LOGGER.error("input file not found");
        } catch (IOException ioe) {
            LOGGER.error("io error while loading data");
        }

        for (WalletType walletType : this.data.getWallets()) {
            WalletType.WalletTypesAssociation.createAssociation(walletType);
        }

        for (User user : this.data.getUsers()) {
            user.initWallets();
        }

        return this.data;
    }

    private class SpecificClassExclusionStrategy implements ExclusionStrategy {
        private final Class<?> excludedThisClass;

        public SpecificClassExclusionStrategy(Class<?> excludedThisClass) {
            this.excludedThisClass = excludedThisClass;
        }

        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }

        public boolean shouldSkipField(FieldAttributes f) {
            return f.getName().equals("wallets");
        }
    }

}

