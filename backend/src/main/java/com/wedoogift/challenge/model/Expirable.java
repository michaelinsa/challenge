package com.wedoogift.challenge.model;

public interface Expirable {
    public void calculateExpirationDate();
}
