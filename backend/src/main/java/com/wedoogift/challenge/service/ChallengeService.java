package com.wedoogift.challenge.service;

import com.wedoogift.challenge.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ChallengeService {

    static Logger LOGGER = LoggerFactory.getLogger(ChallengeService.class);

    @Value("${application.level1.input}")
    String pathInputLevel1;

    @Value("${application.level1.output}")
    String pathOutputLevel1;

    @Value("${application.level1.output2021}")
    String pathOutputLevel12021;

    @Value("${application.level2.input}")
    String pathInputLevel2;

    @Value("${application.level2.output}")
    String pathOutputLevel2;

    @Autowired
    JsonService jsonService;

    @Autowired
    UserService userService;

    @Autowired
    CompanyService companyService;

    @Autowired
    DistributionService distributionService;

    @Autowired
    CalendarService calendarService;

    @Autowired
    DataService dataService;

    //@PostConstruct
    public void solveLevel1() {

        //can't get the exact expected output because all the distributions are expired at this moment (2021-12-10)
        LOGGER.info("loading data level 1");
        Data inputData1 = jsonService.loadData(pathInputLevel1);
        inputData1.addDistribution(this.distributionService.distribute(1, inputData1.getCompanyById(1), inputData1.getUserById(1), new GiftCard(50, this.calendarService.getDateFromString("2020-09-16"))));
        inputData1.addDistribution(this.distributionService.distribute(2, inputData1.getCompanyById(1), inputData1.getUserById(2), new GiftCard(100, this.calendarService.getDateFromString("2020-08-01"))));
        inputData1.addDistribution(this.distributionService.distribute(3, inputData1.getCompanyById(2), inputData1.getUserById(3), new GiftCard(1000, this.calendarService.getDateFromString("2020-05-01"))));
        jsonService.saveData(inputData1, pathOutputLevel1);

        Data inputData2 = jsonService.loadData(pathInputLevel1);
        inputData2.addDistribution(this.distributionService.distribute(1, inputData2.getCompanyById(1), inputData2.getUserById(1), new GiftCard(50, this.calendarService.getDateFromString("2021-09-16"))));
        inputData2.addDistribution(this.distributionService.distribute(2, inputData2.getCompanyById(1), inputData2.getUserById(2), new GiftCard(100, this.calendarService.getDateFromString("2021-08-01"))));
        inputData2.addDistribution(this.distributionService.distribute(3, inputData2.getCompanyById(2), inputData2.getUserById(3), new GiftCard(1000, this.calendarService.getDateFromString("2021-05-01"))));
        jsonService.saveData(inputData2, pathOutputLevel12021);

    }

    public void solveLevel2() {

        LOGGER.info("loading data level 2");
        Data inputData = jsonService.loadData(pathInputLevel2);

        inputData.addDistribution(this.distributionService.distribute(1, inputData.getCompanyById(1), inputData.getUserById(1), new GiftCard(50, this.calendarService.getDateFromString("2021-09-16"))));
        inputData.addDistribution(this.distributionService.distribute(2, inputData.getCompanyById(1), inputData.getUserById(2), new GiftCard(100, this.calendarService.getDateFromString("2021-08-01"))));
        inputData.addDistribution(this.distributionService.distribute(3, inputData.getCompanyById(2), inputData.getUserById(3), new GiftCard(1000, this.calendarService.getDateFromString("2021-05-01"))));
        inputData.addDistribution(this.distributionService.distribute(4, inputData.getCompanyById(1), inputData.getUserById(1), new FoodCard(250, this.calendarService.getDateFromString("2021-01-01"))));
        LOGGER.info("Saving data level 2");
        this.jsonService.saveData(inputData, pathOutputLevel2);
        LOGGER.info("done level 2");
    }

    @PostConstruct
    public void level3() {
        this.dataService.setData(jsonService.loadData(pathInputLevel2));
    }

}
