package com.wedoogift.challenge.model.exception;

public class NotEnoughMoneyException extends Exception{

    public NotEnoughMoneyException(String errorMessage){
        super(errorMessage);
    }

}
