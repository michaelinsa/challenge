package com.wedoogift.challenge.model;

import java.time.Instant;
import java.util.Date;

public class Card implements Expirable{

    Integer amount;
    Date startDate;
    Date expirationDate;
    int walletId;

    public Card(){}

    public Card(Integer amount, Date startDate, int walletId) {
        this.amount = amount;
        this.startDate = startDate;
        this.calculateExpirationDate();
        this.setWalletId(walletId);
    }

    public Card(Integer amount, Date startDate) {
        this.amount = amount;
        this.startDate = startDate;
        this.calculateExpirationDate();
    }

    public Card(Integer amount, Date startDate, Date endDate) {
        this.amount = amount;
        this.startDate = startDate;
        this.expirationDate = endDate;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getWalletId() {
        return walletId;
    }

    public void setWalletId(int walletId) {
        this.walletId = walletId;
    }

    public boolean isExpired() {
        return this.startDate != null && this.expirationDate != null && expirationDate.compareTo(Date.from(Instant.now())) < 0;
    }

    public void calculateExpirationDate() {
        this.expirationDate = null;
    };
}
