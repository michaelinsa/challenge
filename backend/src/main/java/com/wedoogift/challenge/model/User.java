package com.wedoogift.challenge.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class User{

    static final Logger LOGGER = LoggerFactory.getLogger(User.class);

    private int id;
    private List<Wallet> balance;
    private transient List<Card> cards;

    public User() {
        this.cards = new ArrayList<>();
        this.balance = new ArrayList<>();
    }

    public User(int id) {
        this.id = id;
        this.cards = new ArrayList<>();
        this.balance = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public List<Wallet> getBalance() {
        return this.balance;
    }

    public Integer getWalletAmount(int idWallet) {
        return this.balance.stream().filter(wallet -> wallet.getWallet_id() == idWallet).collect(Collectors.toList()).get(0).getAmount();
    }

    private Wallet getWalletById(int idWallet) {
        return this.balance.stream()
                .filter(wallet -> wallet.getWallet_id() == idWallet)
                .collect(Collectors.toList())
                .get(0);
    }


    public void addCard(Card card) {
        this.cards.add(card);
    }

    private boolean hasWalletWithId(int walletId) {
        for (Wallet wallet : this.balance) {
            if (wallet.getWallet_id() == walletId){
                return true;
            }
        }
        return false;
    }

    public List<Wallet> computeBalance() {


        for (Wallet wallet : this.balance) {
            wallet.setAmount(0);
        }

        for (Card card : this.cards) {
            if (!this.hasWalletWithId(card.getWalletId())) {
                Wallet wallet = new Wallet(card.getWalletId());
                this.balance.add(wallet);
            }

            if(!card.isExpired()){
                this.getWalletById(card.getWalletId()).addAmount(card.getAmount());
            }

        }

        return this.balance;

    }

    public void initWallets() {
        //init user in case there is a wallet in input data :
        //we don't know the expiration date so we add an "unlimited" card
        for (Wallet wallet : this.balance) {
            this.cards.add(new Card(wallet.getAmount(), null, wallet.getWallet_id()));
        }
    }

    public Integer getFoodBalance() {
        try {
            return this.balance.stream()
                    .filter(wallet -> wallet.getWallet_id() == WalletType.WalletTypesAssociation.getWalletId("FOOD")).collect(Collectors.toList()).get(0).getAmount();
        }
        catch (NullPointerException npe) {
            return 0;
        }
        catch (ArrayIndexOutOfBoundsException ae) {
            return 0;
        }
    }

    public Integer getGiftBalance() {
        try {
            return this.balance.stream()
                    .filter(wallet -> wallet.getWallet_id() == WalletType.WalletTypesAssociation.getWalletId("GIFT")).collect(Collectors.toList()).get(0).getAmount();
        }
        catch (NullPointerException npe) {
            return 0;
        }
        catch (IndexOutOfBoundsException ie) {
            return 0;
        }
    }


}
