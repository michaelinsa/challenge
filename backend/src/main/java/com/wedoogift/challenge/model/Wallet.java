package com.wedoogift.challenge.model;

public class Wallet {

    private int wallet_id;
    private Integer amount;

    public Wallet(int wallet_id) {
        this.wallet_id = wallet_id;
        this.setAmount(0);
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void addAmount(Integer amount){
        this.amount += amount;
    };

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
