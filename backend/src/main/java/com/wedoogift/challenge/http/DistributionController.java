package com.wedoogift.challenge.http;

import com.wedoogift.challenge.model.*;
import com.wedoogift.challenge.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DistributionController {

    static final Logger LOGGER = LoggerFactory.getLogger(DistributionController.class);

    @Value("${application.level3.output}")
    private String outputFile;

    @Autowired
    DistributionService distributionService;

    @Autowired
    DataService dataService;

    @Autowired
    JsonService jsonService;

    @Autowired
    CalendarService calendarService;


    @GetMapping("/distributions")
    public List<Distribution> getAllDistributions(){
        return this.distributionService.getDistributions();
    }

    @PreAuthorize("permitAll")
    @PostMapping("/foodcard")
    public Distribution distributeFoodCard(@RequestBody Distribution distribution){
        LOGGER.info("post foodcard");
        User user = this.dataService.getUserById(distribution.getUser_id());
        Company company = dataService.getCompanyById(distribution.getCompany_id());
        Distribution distribution1 = distributionService.distribute(
                distribution.getId(),
                company,
                user,
                new FoodCard(distribution.getAmount(), distribution.getStart_date())
        );
        this.dataService.addDistribution(distribution1);
        this.jsonService.saveData(dataService.getData(), this.outputFile);

        return distribution1;
    }

    @PostMapping("/giftcard")
    public Distribution distributeGiftCard(@RequestBody Distribution distribution){
        LOGGER.info("post giftcard");
        User user = this.dataService.getUserById(distribution.getUser_id());
        Company company = dataService.getCompanyById(distribution.getCompany_id());
        Distribution distribution1 = distributionService.distribute(
                distribution.getId(),
                company,
                user,
                new GiftCard(distribution.getAmount(), distribution.getStart_date())
        );
        this.dataService.addDistribution(distribution1);
        this.jsonService.saveData(dataService.getData(), this.outputFile);

        return distribution1;
    }

}
