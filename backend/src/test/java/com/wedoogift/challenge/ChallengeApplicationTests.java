package com.wedoogift.challenge;

import com.wedoogift.challenge.model.*;
import com.wedoogift.challenge.service.DistributionService;
import com.wedoogift.challenge.service.JsonService;
import com.wedoogift.challenge.service.CalendarService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.PostConstruct;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("test")
class ChallengeApplicationTests {

    static Logger LOGGER = LoggerFactory.getLogger(ChallengeApplicationTests.class);

    @Value("${application.test.input}")
    String pathInputLevel1;

    @Value("${application.test.output}")
    String pathOutputLevel1;

    @Autowired
    JsonService jsonService;

    @Autowired
    CalendarService calendarService;

    @Autowired
    DistributionService distributionService;

    @PostConstruct
    //needed if we don't load data from file
    void initWalletTypes() {
        WalletType.WalletTypesAssociation.createAssociation(new WalletType(1, "gift wallet", "GIFT"));
        WalletType.WalletTypesAssociation.createAssociation(new WalletType(2, "food wallet", "FOOD"));
    }

    @Test
    void loadingTest() {
        Data data = this.jsonService.loadData(pathInputLevel1);
        assert (data.getUsers().size() == 3);
        assert (data.getCompanies().size() == 2);
        assert (data.getDistributions().size() == 0);
    }

    @Test
    void savingTest() {
        File outPutFile = new File(pathOutputLevel1);
        Data data = new Data();
        User user1 = new User(1);
        User user2 = new User(2);
        Company company1 = new Company(1, "Google", 3000);
        data.addUser(user1);
        data.addUser(user2);
        data.addCompany(company1);
        jsonService.saveData(data, pathOutputLevel1);
        outPutFile = new File(pathOutputLevel1);
        assert (outPutFile.exists());
        outPutFile.delete();
    }


    @Test
    void initCompanyTest() {
        Company company = new Company(1, "Burger King", 12345);
        assert (company.getBalance().equals(12345));
    }

    @Test
    void isExpiredTest() throws ParseException {
        GiftCard giftCard1 = new GiftCard(50, this.calendarService.getDateFromString("2000-09-05"));
        GiftCard giftCard2 = new GiftCard(100, this.calendarService.getDateFromString("2021-09-15"));
        FoodCard foodCard1 = new FoodCard(100, this.calendarService.getDateFromString("2020-12-30"));
        FoodCard foodCard2 = new FoodCard(100, this.calendarService.getDateFromString("2021-01-01"));
        assert (giftCard1.isExpired());
        assert (!giftCard2.isExpired());
        assert (foodCard1.isExpired());
        assert (!foodCard2.isExpired());
    }


    @Test
    void calculateBalanceTest() {

        User user = new User(1);
        GiftCard giftCard = new GiftCard(50, this.calendarService.getDateFromString("2021-12-02"));
        assert (user.getBalance().size() == 0);
        user.addCard(giftCard);
        user.computeBalance();
        assert (user.getBalance().size() == 1);
        FoodCard foodCard = new FoodCard(3000, this.calendarService.getDateFromString("2021-12-02"));
        user.addCard(foodCard);
        user.computeBalance();
        assert (user.getBalance().size() == 2);
        assert (user.getFoodBalance() == 3000);
        assert (user.getGiftBalance() == 50);
    }


    @Test
    void expirationDateTest() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        GiftCard giftCard = new GiftCard(150, this.calendarService.getDateFromString("2020-09-25"));
        FoodCard foodCard = new FoodCard(1500, this.calendarService.getDateFromString("2021-09-25"));
        assert (giftCard.getExpirationDate().equals(formatter.parse("2021-09-24")));
        assert (foodCard.getExpirationDate().equals(formatter.parse("2022-02-28")));

    }

    @Test
    void expirationTest() throws ParseException {
        User user = new User(1);
        GiftCard giftCard1 = new GiftCard(100, this.calendarService.getDateFromString("2021-11-03"));
        GiftCard giftCard2 = new GiftCard(50, this.calendarService.getDateFromString("2000-09-23"));
        FoodCard foodCard1 = new FoodCard(5000, this.calendarService.getDateFromString("2021-01-01"));
        FoodCard foodCard2 = new FoodCard(123, this.calendarService.getDateFromString("2020-12-31"));
        user.addCard(giftCard1);
        user.addCard(giftCard2);
        user.addCard(foodCard1);
        user.addCard(foodCard2);
        user.computeBalance();
        assert (user.getGiftBalance().equals(100));
        assert (user.getFoodBalance().equals(5000));
    }

    @Test
    void distributionTest() {
        User user = new User(1);
        Company company = new Company(1, "Google", 1000);
        Distribution distribution = distributionService.distribute(1, company, user, new GiftCard(500, this.calendarService.getDateFromString("2021-10-01")));
        assert (user.getGiftBalance() == 500);
        assert (company.getBalance() == 500);
        assert (distribution.getCompany_id() == company.getId());
        assert (distribution.getUser_id() == user.getId());
        assert (distribution.getAmount() == 500);
        assert (distribution.getStart_date().equals(this.calendarService.getDateFromString("2021-10-01")));
        assert (distribution.getEnd_date().equals(this.calendarService.getDateFromString("2022-09-30")));
        Distribution distribution2 = distributionService.distribute(2, company, user, new FoodCard(250, this.calendarService.getDateFromString("2021-10-12")));
        assert (user.getFoodBalance() == 250);
        assert (company.getBalance() == 250);
        assert (distribution2.getEnd_date().equals(this.calendarService.getDateFromString("2022-02-28")));
    }

    @Test
    void exactAmountDistributionTest() {
        User user = new User(1);
        Company company = new Company(1, "IBM", 500);
        Distribution distribution = distributionService.distribute(1, company, user, new GiftCard(500, this.calendarService.getDateFromString("2021-02-20")));
        assert(user.getGiftBalance().equals(500));
        assert(company.getBalance() == 0);
        assert (distribution.getCompany_id() == company.getId());
        assert (distribution.getUser_id() == user.getId());
        assert (distribution.getAmount() == 500);
        assert (distribution.getStart_date().equals(this.calendarService.getDateFromString("2021-02-20")));
        assert (distribution.getEnd_date().equals(this.calendarService.getDateFromString("2022-02-19")));
    }

    @Test
    void failedDistributionTest() {
        User user = new User(1);
        Company company = new Company(1, "IBM", 500);
        Distribution distribution = distributionService.distribute(1, company, user, new GiftCard(1000, this.calendarService.getDateFromString("2021-10-01")));
        Data data = new Data();
        data.addUser(user);
        data.addCompany(company);
        assert (distribution == null);
        assert (user.getGiftBalance() == 0);
        assert (company.getBalance() == 500);
        data.addDistribution(distribution);
        assert (data.getDistributions().size() == 0);
    }



    //---------------------------------------LEVEL 2--------------------------------------------------------------------------




}
