package com.wedoogift.challenge.model;

import java.util.Date;

public class Distribution {
    private int id;
    private int wallet_id;
    private Integer amount;
    private Date start_date;
    private Date end_date;

    //to have correct Json
    private int company_id;
    private int user_id;

    public Distribution(int id, Integer amount, Date start_date, int userId, int companyId, int wallet_id) {
        this.id = id;
        this.amount = amount;
        this.start_date = start_date;
        this.user_id = userId;
        this.company_id = companyId;
        this.wallet_id = wallet_id;
    }

    public int getId() {
        return id;
    }

    public Integer getAmount() {
        return amount;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setEnd_date(Date endDate) {
        this.end_date = endDate;
    }
    public Date getEnd_date() {
        return end_date;
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public int getCompany_id() {
        return company_id;
    }

    public int getUser_id() {
        return user_id;
    }
}
