package com.wedoogift.challenge.model;

import java.util.Calendar;
import java.util.Date;

public class GiftCard extends Card {

    public GiftCard(Integer amount, Date startDate) {
        super(amount, startDate);
        this.setWalletId(WalletType.WalletTypesAssociation.getWalletId("GIFT"));
    }

    public void calculateExpirationDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.startDate);
        calendar.add(Calendar.DAY_OF_YEAR, 364);
        this.expirationDate = calendar.getTime();
    }
}
