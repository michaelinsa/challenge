package com.wedoogift.challenge.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;

public class FoodCard extends Card {

    static final Logger LOGGER = LoggerFactory.getLogger(FoodCard.class);

    public FoodCard(Integer amount, Date startDate) {
        super(amount, startDate);
        this.setWalletId(WalletType.WalletTypesAssociation.getWalletId("FOOD"));
    }

    @Override
    public void calculateExpirationDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.startDate);
        calendar.add(Calendar.YEAR, 1);
        calendar.set(Calendar.MONTH, Calendar.FEBRUARY);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        this.expirationDate = calendar.getTime();
        LOGGER.info("expiration date : " + this.expirationDate.toString());
    }
}
