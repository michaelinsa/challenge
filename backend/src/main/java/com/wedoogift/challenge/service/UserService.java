package com.wedoogift.challenge.service;

import com.wedoogift.challenge.model.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserService {
    private Map<Integer, User> users;

    public UserService(){
        this.users = new HashMap<>();
    }

    public User getUserById(int userId){
        return this.users.get(new Integer(userId));
    }

    public List<User> getAllUsers(){
        return this.users.values().stream().collect(Collectors.toList());
    }

    public User addUser(User user) {
        return this.users.put(user.getId(), user);
    }
}
