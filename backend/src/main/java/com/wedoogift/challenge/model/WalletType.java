package com.wedoogift.challenge.model;

import java.util.HashMap;
import java.util.Map;

public class WalletType {
    private int id;
    private String name;
    private String type;

    public WalletType(int id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public static class WalletTypesAssociation {

        private static Map<String, Integer> walletTypes = new HashMap<>();

        public static void createAssociation(WalletType walletType) {
            walletTypes.put(walletType.getType(), walletType.getId());
        }

        public static int getWalletId(String walletType){
            return walletTypes.get(walletType);
        }

    }
}
