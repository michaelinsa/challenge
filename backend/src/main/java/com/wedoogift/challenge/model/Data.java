package com.wedoogift.challenge.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Data {
    List<WalletType> wallets;
    List<Company> companies;
    List<User> users;
    List<Distribution> distributions;

    public Data() {
        this.users = new ArrayList<>();
        this.companies = new ArrayList<>();
        this.distributions = new ArrayList<>();
        this.wallets = new ArrayList<>();
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }

    public List<WalletType> getWallets() {
        return wallets;
    }

    public void addUser(User user){
        this.users.add(user);
    }

    public void addCompany(Company company) {
        this.companies.add(company);
    }

    public void addDistribution(Distribution distribution) {
        if (distribution == null) {
            return;
        }
        this.distributions.add(distribution);
    }

    public User getUserById(int id) {
        return this.users.stream().filter(user -> user.getId() == id).collect(Collectors.toList()).get(0);
    }

    public Company getCompanyById(int id) {
        return this.companies.stream().filter(company -> company.getId() == id).collect(Collectors.toList()).get(0);
    }
}
